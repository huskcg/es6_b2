let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let renderGlasses = (dataGlasses) => {
  let html = "";
  dataGlasses.forEach((glass) => {
    html += `
        <div class="glass-item" id="${glass.id}" style="width:30%">
          <img src="${glass.src}" style="width:100%;height:auto; cursor:pointer"/>
        </div>
      `;
  });
  return html;
};

let glassesList = renderGlasses(dataGlasses);
document.getElementById("vglassesList").innerHTML = glassesList;

let glasses = document.querySelectorAll(".glass-item");

glasses.forEach((glass) => {
  glass.addEventListener("click", function () {
    // Lấy thông tin của glass
    let glassId = this.getAttribute("id");
    let glassInfo = dataGlasses.find((item) => item.id === glassId);

    // Hiển thị image lên avatar
    document.getElementById(
      "avatar"
    ).innerHTML = `<img src="${glassInfo.virtualImg}" class="glass" id="${glassInfo.id}"/>`;

    // Hiển thị thông tin của glass
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = `    
                                                            <h4>${glassInfo.name} - ${glassInfo.brand} (${glassInfo.color})</h4>
                                                            <p class="bg-danger d-inline p-1 rounded">$${glassInfo.price}</p>
                                                            <span class="text-success">Stocking</span>
                                                            <p> ${glassInfo.description}</p>
                                                        `;
  });
});

function removeGlasses(flag) {
  let glass = document.querySelectorAll(".glass");

  if (glass.length != 0) {
    if (flag) {
      document.querySelector(".glass").style.display = "block";
      document.querySelector(".vglasses__info").style.display = "block";
    } else {
      document.querySelector(".glass").style.display = "none";
      document.querySelector(".vglasses__info").style.display = "none";
    }
  } else {
  }
}
window.removeGlasses = removeGlasses;
